# XeTeX + xeCJK 中文教學

## 編譯方式
* make
* 使用任何 TeX Editor, 啟用 xelatex 的 shell-escape 選項
  ```
  xelatex -synctex=1 -shell-escape -interaction=nonstopmode %.tex
  ```
